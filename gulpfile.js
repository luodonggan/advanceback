var gulp = require('gulp');
var usemin = require('gulp-usemin');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var removelogs = require('gulp-removelogs');

/**
 * demo
 */
gulp.task('demo-clean', function(){
    gulp.src('./dist').pipe(clean())
})

gulp.task('demo-move', function(){
    gulp.src(['demo/index.html'])
        .pipe(gulp.dest('dist/demo'))

    gulp.src(['assets/admin/layout2/img/**'])
        .pipe(gulp.dest('dist/demo/img'))
        
    gulp.src(['assets/global/plugins/font-awesome/fonts/**'])
        .pipe(gulp.dest('dist/demo/fonts'))
        
    gulp.src(['assets/global/plugins/simple-line-icons/fonts/**'])
        .pipe(gulp.dest('dist/demo/css/fonts'))

    gulp.src(['assets/global/plugins/simple-line-icons/fonts/**'])
        .pipe(gulp.dest('dist/demo/css/fonts'))
        
    gulp.src(['assets/global/plugins/bootstrap/fonts/**'])
        .pipe(gulp.dest('dist/demo/fonts'))
        
    gulp.src(['assets/global/plugins/select2/**.png','assets/global/plugins/select2/**.gif'])
        .pipe(gulp.dest('dist/demo/css'))

    gulp.src(['assets/global/plugins/uniform/images/**'])
        .pipe(gulp.dest('dist/demo/images'))
        
    gulp.src(['assets/global/img/**'])
        .pipe(gulp.dest('dist/demo/img'))
        
    gulp.src(['assets/admin/pages/img/**'])
        .pipe(gulp.dest('dist/demo/css/img'))
        
    gulp.src(['assets/global/plugins/icheck/skins/minimal/**'])
        .pipe(gulp.dest('dist/demo/css/icheck'))

    gulp.src(['My97DatePicker/lang/**'])
        .pipe(gulp.dest('dist/demo/lang'))

    gulp.src(['My97DatePicker/skin/**'])
        .pipe(gulp.dest('dist/demo/skin'))

    gulp.src(['My97DatePicker/calendar.js'])
        .pipe(gulp.dest('dist/demo'))

    gulp.src(['demo/doc/**'])
        .pipe(gulp.dest('dist/demo/doc'))

    gulp.src(['demo/image/**'])
        .pipe(gulp.dest('dist/demo/image'))

    gulp.src(['demo/**.json'])
        .pipe(gulp.dest('dist/demo'))

    gulp.src(['bundle/*.png','bundle/*.gif','bundle/*.jpg','bundle/*.JPG'], {base: './bundle/'})
        .pipe(gulp.dest('dist/demo/resource'));
})

gulp.task('demo-rev', function(){

    gulp.src('./demo/index.html')
        .pipe(usemin({
            css_style: [ 'concat', rev()],
            css_plugins: ['concat', rev()],
            css_mark:['concat', rev()],
            css_code:['concat', rev()],
            
            js_common: [ rev()],
            js_plugins: [rev()],
            js_mark:[rev()],
            js_code: [rev()],
            js_index: [removelogs(), uglify(), rev()],
        }))
        .pipe(gulp.dest('dist/demo'))

    gulp.src('./demo/login.html')
        .pipe(usemin({
            css_style: [ 'concat', rev()],
            css_plugins_login: ['concat', rev()],
            
            js_common: [ rev()],
            js_plugins_login: [rev()],
            js_login: [removelogs(), uglify(), rev()],
        }))
        .pipe(gulp.dest('dist/demo'))
})
